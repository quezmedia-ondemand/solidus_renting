module SolidusRenting
  class Engine < Rails::Engine
    require 'spree/core'
    isolate_namespace Spree
    engine_name 'solidus_renting'

    # use rspec for tests
    config.generators do |g|
      g.test_framework :rspec
    end

    def self.activate
      Dir.glob(File.join(File.dirname(__FILE__), '../../app/**/*_decorator*.rb')) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end

      # Spree::RoleConfiguration.configure do |config|
      #   config.assign_permissions :admin, [Spree::PermissionSets::RentingOptionDisplay]
      # end
    end

    config.to_prepare(&method(:activate).to_proc)
  end
end
