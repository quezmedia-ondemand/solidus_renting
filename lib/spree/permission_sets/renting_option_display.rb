module Spree
  module PermissionSets
    class RentingOptionDisplay < PermissionSets::Base
      def activate!
        can [:display, :admin, :edit], Spree::RentingOption
      end
    end
  end
end
