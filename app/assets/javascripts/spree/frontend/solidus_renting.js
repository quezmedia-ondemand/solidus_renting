// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/frontend/all.js'

Spree.ready(function($) {
  var radios, selectedRadio;
  Spree.updateRentingOption = function(variant) {
    var variantRentingPeriod;
    variantRentingPeriod = variant.data('rentingperiod');
    if (variantRentingPeriod.length > 0) {
      return ($('.renting-option')).text("(per month)");
    }else{
      return ($('.renting-option')).text('');
    }
  };
  radios = $('#product-variants input[type="radio"]');
  if (radios.length > 0) {
    selectedRadio = $('#product-variants input[type="radio"][checked="checked"]');
    Spree.updateRentingOption(selectedRadio);
  }
  return radios.click(function(event) {
    return Spree.updateRentingOption($(this));
  });
});
