Deface::Override.new(:virtual_path => "spree/shared/_order_details",
                     :name => "variant_order_details_renting_alert",
                     :insert_bottom => "tr[data-hook='order_details_line_item_row'] td[data-hook='order_item_description']",
                     :partial => "spree/shared/renting_alert",
                     :original => '3f05ddbe6c4501b28f24d05797b0bff647343077',
                     :disabled => false)
