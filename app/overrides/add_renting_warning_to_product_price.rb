Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                     :name => "cart_renting_alert_at_product_price",
                     :insert_after => "div#product-price span.price",
                     :partial => "spree/products/cart_renting_alert_price",
                     :original => '6383bce1e71cbee34023d0fb11b21619964f5bd1',
                     :disabled => false)

Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                    :name => "radio_variant_replacement",
                    :replace => "erb[loud]:contains('radio_button_tag')",
                    :partial => "spree/products/radio_replacement",
                    :disabled => false)
