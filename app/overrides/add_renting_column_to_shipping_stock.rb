Deface::Override.new(:virtual_path => "spree/checkout/_delivery",
                     :name => "line_item_column_shipping_stock",
                     :replace => "div.shipment tr.stock-item td.item-name",
                     :partial => "spree/checkout/renting_column",
                     :disabled => false)
