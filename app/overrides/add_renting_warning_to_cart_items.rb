Deface::Override.new(:virtual_path => "spree/orders/_line_item",
                     :name => "cart_renting_alert",
                     :insert_after => "td span.line-item-description",
                     :partial => "spree/orders/cart_renting_alert",
                     :disabled => false)
