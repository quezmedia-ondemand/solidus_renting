Deface::Override.new(
  virtual_path: "spree/products/_cart_form",
  name: "removing_variants_label",
  replace_contents: "div[data-hook='inside_product_cart_form'] div#product-variants h6.product-section-title",
  text: '<h6>Options</h6>',
  disabled: false)
