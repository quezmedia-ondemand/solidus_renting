Deface::Override.new(:virtual_path => "spree/admin/variants/_form",
                     :name => "variant_renting_option",
                     :insert_after => "div[data-hook='variants'] .row .col-3:last",
                     :partial => "spree/admin/variants/renting_option_field",
                     :original => "5aea7c87ce554d9287d1e9e983985fba0089be89",
                     :disabled => false)

Deface::Override.new(:virtual_path => "spree/admin/products/new",
                    :name => "master_variant_renting_options",
                    :insert_after => "fieldset[data-hook='new_product'] div[data-hook='new_product_attrs']",
                    :partial => "spree/admin/products/renting_option_fields",
                    :original => '99aa040cc2c4279afc88d129238c859793302b52',
                    :disabled => false)

# Deface::Override.new(:virtual_path => "spree/admin/products/_form",
#                     :name => "edit_master_variant_renting_option",
#                     :insert_after => "div[data-hook='admin_product_form_tax_category']",
#                     :partial => "spree/admin/products/edit_renting_option_field",
#                     :original => 'fe0eb9d85bee5c633944a11cab86c941df2a8ca3',
#                     :disabled => false)
