Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                     :name => "variant_renting_alert",
                     :insert_after => "div#product-variants ul li span.variant-description",
                     :partial => "spree/products/renting_alert",
                     :original => "aa934d84eacb4de1ff2533e413d6b66509ed29bd",
                     :disabled => false)
