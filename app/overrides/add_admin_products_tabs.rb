Deface::Override.new(:virtual_path => "spree/admin/shared/_product_sub_menu",
                     :name => "product_renting_options",
                     :insert_bottom => "[data-hook='admin_product_sub_tabs']",
                     :partial => "spree/admin/shared/product_renting_options_tab",
                     :original => "d1a70da8f31da0a082c6c5333d9837dcaca65c65",
                     :disabled => false)
