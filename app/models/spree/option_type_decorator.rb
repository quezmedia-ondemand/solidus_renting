module Spree
  OptionType.class_eval do
    scope :without_renting_values,     ->(){ includes(:option_values).where(spree_option_values: { is_renting: false }) }
  end
end
