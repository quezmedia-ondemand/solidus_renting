module Spree
  Product.class_eval do
    [:renting_option, :renting_option_id,
      :spree_renting_option_id].each do |attr|
      delegate :"#{attr}", :"#{attr}=", to: :find_or_build_master
    end

    attr_accessor :renting_options

    has_many :option_types,             ->(){ without_renting_values },
                                        through: :product_option_types

    accepts_nested_attributes_for :master, allow_destroy: true, reject_if: :all_blank

    alias_method :old_variants_and_option_values_for, :variants_and_option_values_for
    def variants_and_option_values_for(pricing_options = Spree::Config.default_pricing_options)
      inner_variants = old_variants_and_option_values_for(pricing_options)
      inner_variants.sort_by{|v| v.sku.include?('buy') || v.option_values.map(&:name).include?('buy') ? 0 : 1 }
    end
  end
end
