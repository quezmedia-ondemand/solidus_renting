module Spree
  LineItem.class_eval do
    has_one :renting_option, through: :variant
    before_create :set_renting_end_date, if: Proc.new{ |l| l.renting_option.present? }

    scope :rentable, ->(){ where.not(renting_end_date: nil) }

    private
    def set_renting_end_date
      self.renting_end_date = eval("#{renting_option.period_quantity}.#{renting_option.normatized_period_type}").from_now.to_date
    end
  end
end
