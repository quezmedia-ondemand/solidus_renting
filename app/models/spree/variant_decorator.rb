module Spree
  Variant.class_eval do
    belongs_to :renting_option,        class_name: Spree::RentingOption,
                                       foreign_key: 'spree_renting_option_id'

    alias_attribute :renting_option_id, :spree_renting_option_id
    alias_attribute :renting_option_id=, :spree_renting_option_id=

    def options_text
      values = option_values.includes(:option_type)
      values_arr = values.sort_by do |option_value|
        option_value.option_type.position
      end.to_a.reject{|v| v.is_renting? }.map do |ov|
        "#{ov.option_type.presentation}: #{ov.presentation}"
      end

      list_str = values_arr.to_sentence({ words_connector: ", ", two_words_connector: ", " })
      purchasing_type_value = values.select{|v| v.is_renting? }.first
      list_str += ". " unless list_str.empty?
      list_str += (purchasing_type_value && purchasing_type_value.name == 'rent' ? "Rent" : "Buy now")
      list_str
    end
  end
end
