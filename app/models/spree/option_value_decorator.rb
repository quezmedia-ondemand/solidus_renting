module Spree
  OptionValue.class_eval do
    scope :not_renting,     ->(){ where(is_renting: false) }
  end
end
