module Spree
  class RentingOption < Spree::Base
    acts_as_list
    after_save :touch_all_variants

    has_many :variants, class_name: Spree::Variant,
                        foreign_key: 'spree_renting_option_id',
                        inverse_of: :renting_option,
                        dependent: :nullify

    validates :name, presence: true, uniqueness: { allow_blank: true }
    validates :period_quantity, :period_type, presence: true
    validates :period_quantity, numericality: { only_integer: true,
     greater_than_or_equal_to: 1 }

    enum period_type: { month: 0, day: 1, week: 2, year: 3 }

    default_scope { order(position: :asc) }

    def presentation
      "#{period_quantity} #{normatized_period_type}"
    end

    def normatized_period_type
      period_quantity > 1 || period_quantity == 0 ? period_type.pluralize : period_type.singularize
    end

    private
    def touch_all_variants
      variants.update_all(updated_at: Time.current)
    end
  end
end
