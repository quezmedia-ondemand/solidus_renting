module Spree
  module Admin
    class RentingOptionsController < ResourceController

      def update_values_positions
        params[:positions].each do |id, index|
          Spree::RentingOption.where(id: id).update_all(position: index)
        end

        respond_to do |format|
          format.html { redirect_to admin_product_variants_url(params[:product_id]) }
          format.js { render plain: 'Ok' }
        end
      end

      private

      def location_after_save
        edit_admin_renting_option_url(@renting_option)
      end

      def load_product
        @product = Spree::Product.find_by_param!(params[:product_id])
      end
    end
  end
end
