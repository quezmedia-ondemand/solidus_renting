module Spree
  module Admin
    VariantsController.class_eval do
      update.after :attach_renting_option_values
      create.after :attach_renting_option_values

      private
      def attach_renting_option_values
        if permitted_resource_params[:spree_renting_option_id].present?
          renting_values = Spree::OptionValue.where(spree_option_values: { is_renting: true })
          buy_ov, rent_ov = renting_values.first, renting_values.second
          unless @object.option_values.where(is_renting: true, name: 'rent').any?
            @object.option_values_variants.build(option_value: rent_ov)
          end
          @object.save
        end
      end
    end
  end
end
