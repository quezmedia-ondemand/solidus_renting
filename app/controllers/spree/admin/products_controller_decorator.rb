module Spree
  module Admin
    ProductsController.class_eval do
      create.before :attach_renting_definitions
      update.before :attach_renting_definitions
      create.after :create_renting_variants_if_needed

      private
      def attach_renting_definitions
        renting_ot_ids = Spree::OptionType.includes(:option_values)
          .where(spree_option_values: { is_renting: true }).pluck(:id)
        @object.option_type_ids |= renting_ot_ids
      end

      def create_renting_variants_if_needed
        if has_any_renting_options_params?
          master_attributes = @object.master.attributes.except('id', 'created_at', 'deleted_at', 'is_master')
          buy_ov = Spree::OptionValue.find_by(name: 'buy', is_renting: true)
          rent_ov = Spree::OptionValue.find_by(name: 'rent', is_renting: true)
          permitted_resource_params[:renting_options].reject(&:blank?).each_with_index do |ro, index|
            renting_option = Spree::RentingOption.find(ro)
            renting_variant = build_common_variant(master_attributes, rent_ov)
            renting_variant.sku = renting_variant.sku + "-#{renting_option.period_quantity}-#{renting_option.period_type.downcase}"
            renting_variant.renting_option_id = renting_option.id
            renting_variant.position = index + 2
            renting_variant.save
          end
          buying_variant = build_common_variant(master_attributes, buy_ov)
          buying_variant.sku = buying_variant.sku + "-buy"
          buying_variant.position = 1
          buying_variant.save
        end
      end

      def build_common_variant(master_attributes, option_value)
        temp_variant = @object.variants.build(master_attributes)
        temp_variant.price = @object.master.price
        temp_variant.option_values_variants.build(option_value: option_value)
        temp_variant
      end

      def has_any_renting_options_params?
        permitted_resource_params[:renting_options].present? &&
          permitted_resource_params[:renting_options].any?(&:present?)
      end
    end
  end
end
