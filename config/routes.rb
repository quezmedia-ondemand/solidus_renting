Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :renting_options do
      collection do
        post :update_positions
        post :update_values_positions
      end
    end
  end
end
