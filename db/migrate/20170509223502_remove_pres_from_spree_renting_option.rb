class RemovePresFromSpreeRentingOption < ActiveRecord::Migration[5.0]
  def change
    remove_column :spree_renting_options, :presentation, :string
  end
end
