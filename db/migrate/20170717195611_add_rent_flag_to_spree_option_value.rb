class AddRentFlagToSpreeOptionValue < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_option_values, :is_renting, :boolean, default: false
  end
end
