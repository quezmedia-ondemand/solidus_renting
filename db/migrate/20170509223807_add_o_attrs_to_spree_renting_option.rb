class AddOAttrsToSpreeRentingOption < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_renting_options, :period_type, :integer
    add_column :spree_renting_options, :period_quantity, :integer
  end
end
