class AddRentingOptionToSpreeVariant < ActiveRecord::Migration[5.0]
  def change
    add_reference :spree_variants, :spree_renting_option, foreign_key: true, index: true
  end
end
