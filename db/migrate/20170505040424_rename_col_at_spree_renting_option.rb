class RenameColAtSpreeRentingOption < ActiveRecord::Migration[5.0]
  def change
    rename_column :spree_renting_options, :representation, :presentation
  end
end
