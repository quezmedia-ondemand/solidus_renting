class DropRentingOptionDepTables < ActiveRecord::Migration[5.0]
  def change
    drop_table :spree_variant_renting_options
  end
end
