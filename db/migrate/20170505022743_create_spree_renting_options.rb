class CreateSpreeRentingOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_renting_options do |t|
      t.string :name
      t.string :representation
      t.integer :position

      t.timestamps
    end
  end
end
