class CreateSpreeVariantRentingOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_variant_renting_options do |t|
      t.integer :position
      t.references :spree_variant, foreign_key: true, index: true
      t.references :spree_renting_option, foreign_key: true, index: true

      t.timestamps
    end
  end
end
