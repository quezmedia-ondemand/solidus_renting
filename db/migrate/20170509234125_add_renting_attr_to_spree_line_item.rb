class AddRentingAttrToSpreeLineItem < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_line_items, :renting_end_date, :date
  end
end
